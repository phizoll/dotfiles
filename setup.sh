#!/usr/bin/env bash
########################################
#          _                    _      #
# ___  ___| |_ _   _ _ __   ___| |__   #
#/ __|/ _ \ __| | | | '_ \ / __| '_ \  #
#\__ \  __/ |_| |_| | |_) |\__ \ | | | #
#|___/\___|\__|\__,_| .__(_)___/_| |_| #
#                   |_|                #
########################################
#------------ Variables ---------------#

SCRIPT_LOCATION=$(dirname $(readlink -f $0))

# Colors
COLOR_GRAY="\033[1;38;5;243m"
COLOR_BLUE="\033[1;34m"
COLOR_GREEN="\033[1;32m"
COLOR_RED="\033[1;31m"
COLOR_PURPLE="\033[1;35m"
COLOR_YELLOW="\033[1;33m"
COLOR_NONE="\033[0m"

# Format Functions

title() {
	echo -e "\n${COLOR_PURPLE}$1${COLOR_NONE}"
	echo -e "${COLOR_GRAY}==============================${COLOR_NONE}\n"
}

error() {
	echo -e "${COLOR_RED}Error: ${COLOR_NONE}$1"
	exit 1
}

warning() {
	echo -e "${COLOR_YELLOW}Warning: ${COLOR_NONE}$1"
}

info() {
	echo -e "${COLOR_BLUE}Info: ${COLOR_NONE}$1"
}

success() {
	echo -e "${COLOR_GREEN}$1${COLOR_NONE}"
}


#------------- Backup -------------#

backup () {
	title "Backup"
	if [ -h ~/.bashrc ]; then
		info "No need for backing up"
	else
		cd ~/
		mkdir -p ~/.backup/
		info "Backing up $HOME"
		mv .bashrc .bash_logout .profile .bash_profile ~/.backup/
		mkdir ~/.config
		cd ~/.config
		info "Backing up $HOME/.config"
		mv alacritty btop fish git ranger tmux tmuxp vim starship.toml lf ~/.backup/
	fi
}


#------------ Setup Dirs------------#

run_stow () {
	title "Stowing Dotfiles"
	cd $SCRIPT_LOCATION
	stow . --no-folding -t ~/
	mkdir ~/.config/vim/swap
}

#--------------- Run ------------------#

if ! [ -e "$(which stow)" ];
then
	error "Stow does not exist, install stow (Future Version of this Script may support a way without stow)"
fi


backup
run_stow
success "Dotfiles installed"
