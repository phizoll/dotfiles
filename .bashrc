####################################
#   _               _              #
#  | |__   __ _ ___| |__  _ __ ___ #
#  | '_ \ / _` / __| '_ \| '__/ __|#
# _| |_) | (_| \__ \ | | | | | (__ #
#(_)_.__/ \__,_|___/_| |_|_|  \___|#
####################################

# --- Standard Config ---

# If not running interactively, don't do anything
case $- in
	*i*) ;;
		*) return;;
esac

if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
fi

shopt -s checkwinsize

if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
if [ -d ~/.config/.bashrc.d ]; then
	for rc in ~/.config/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi
unset rc



# --- User Config ---

[[ $- == *i* ]] && stty -ixon

# Environment Config
if ! [[ "$PATH" =~ "$HOME/.local/bin:" ]]
then
	PATH="$HOME/.local/bin:$PATH"
fi
export PATH
export VIMINIT="source ~/.config/vim/vimrc" #change vimrc to config
export LESSHISTFILE=-

export EDITOR='vim'


# History Management
export HISTIGNORE='lsa:ll:[bf]g:lfr:cd *'
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=5000
export HISTFILESIZE=5000
shopt -s histappend
PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"


# 256 Color setting
if [ "$TERM" == "xterm" ] && [ "$COLORTERM" == "gnome-terminal" ]; then
	export TERM=xterm-256color
fi

# Keybindings

bind '"\e[5~": history-search-backward'
bind '"\e[6~": history-search-forward'


# Aliases

alias lsa='ls -alh'
alias ll='ls -lh'
alias mkpass="< /dev/urandom tr -dc A-Za-z0-9 | head -c30"
alias lf='ranger'
alias virsh='virsh --connect qemu:///system'
alias unix-time="date '+%s'"
alias filem='file --mime-type'
alias git-update-dir='for dir in $(find . -name ".git"); do cd ${dir%/*}; echo $dir ; git pull ; cd -; done'
alias dig='dig +nocomment'
alias digm='dig +noall +short'
alias tmphist='export HISTFILE=$XDG_RUNTIME_DIR/bashhist'


## Tmux

alias tmuxm='tmuxp load -d -y main ; tmux att -t main'
alias tmuxls='tmuxp ls'
alias tmuxn='tmux new -ds $(basename $(pwd))'
alias tmuxns='tmux new -ds'

## Productivity

alias greph='history|grep'
alias vimw='vim -c VimwikiIndex'


# Functions

ssh-sconfl () {
	awk "/$1/" RS='\n\n' ORS='\n\n' ~/.ssh/config
}

## Tmux

tmuxl () {

	if [ "$#" -eq "0" ]; then
		echo "Needs an Argument, look in tmuxls"
		return 1
	else
		tmuxp load -d -y $@ ; tmux att -t $@
	fi
}


## History Manipulation

delh () {
	str=$@
	sed -i "/$str/d" $HISTFILE
}

hf-max (){
	history | awk '{$1="";CMD[$0]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | sort -nr | nl
}

hf-min (){
	history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl
}


## Pip Update for User

update-python () {
	for i in $(pip list -o --user | awk 'NR > 2 {print $1}'); do pip install -U $i; done
}


## Pip Update for Python Environments

update-python-pv () {
	for i in $(pip list -ol | awk 'NR > 2 {print $1}'); do pip install -U $1; done
}


# Bash Prompt

eval "$(starship init bash)"
